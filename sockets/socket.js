const { io } = require("../index");
const Band = require("../models/band");
const Bands = require("../models/bands");

const bands = new Bands();

bands.addBand(new Band("Pantera"));
bands.addBand(new Band("Lamb of god"));
bands.addBand(new Band("in flames"));
bands.addBand(new Band("DreamTeather"));

//Mensajes de sockets
io.on("connection", (client) => {
  console.log("Cliente conectado");

  client.emit("bandas-activas", bands.getBands());

  client.on("disconnect", () => {
    console.log("Cliente desconectado");
  });

  client.on("mensaje", (payload) => {
    console.log("Mensaje", payload);

    io.emit("mensaje", { admin: "Nuevo mensaje" });
  });

  // client.on('emitir-mensaje',(payload)=>{
  //   io.emit('nuevo-mensaje',payload); emite a todos
  //   emite a todos menos el que lo emitio
  //   console.log(payload);
  //   client.broadcast.emit('nuevo-mensaje',payload);
  // });

  client.on("vote-band", (payload) => {
    bands.voteBand(payload.id);
    io.emit("bandas-activas", bands.getBands());
  });

  client.on("add-band", (payload) => {
    const newBand = new Band(payload.name);
    bands.addBand(newBand);
    io.emit("bandas-activas", bands.getBands());
  });

  client.on("delete-band", (payload) => {
    bands.deleteBand(payload.id);
    io.emit("bandas-activas", bands.getBands());
  });
});
